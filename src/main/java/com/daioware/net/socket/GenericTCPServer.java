package com.daioware.net.socket;

import java.io.IOException;
import java.util.HashMap;

import com.daioware.stream.Printer;

public abstract class GenericTCPServer extends Thread{
	protected TCPServSocket servSocket;
	private Printer printer;
	private int maxSimultaneousConnections; 
	protected int currentRunningThreads;
	private boolean active=true;
	private HashMap<Long,Thread> sessions;
		
	public GenericTCPServer(TCPServSocket servSocket, Printer printer) {
		this(servSocket,printer,50);
	}

	public GenericTCPServer(TCPServSocket servSocket, Printer printer, int maxSimultaneousConnections) {
		setServSocket(servSocket);
		setPrinter(printer);
		setMaxSimultaneousConnections(maxSimultaneousConnections);
		setName("GenericTCPServer");
		sessions=new HashMap<>();
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
		if(!active) {
			try {
				servSocket.getServSocket().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

	public Printer getPrinter() {
		return printer;
	}

	public void setPrinter(Printer printer) {
		this.printer = printer;
	}

	
	public TCPServSocket getServSocket() {
		return servSocket;
	}

	public void setServSocket(TCPServSocket servSocket) {
		this.servSocket = servSocket;
	}
	public void print(Object o) {
		printer.print(o);		
	}
	public void println(Object o) {
		printer.println(o);		
	}
	public abstract Runnable getRunnable(TCPClientSocket clientSocket);
	
	
	public int getCurrentConnections() {
		return currentRunningThreads;
	}

	public void setCurrentRunningThreads(int currentRunningThreads) {
		this.currentRunningThreads = currentRunningThreads;
	}
	public synchronized void incrementRunningThreads() {
		setCurrentRunningThreads(getCurrentConnections()+1);
	}
	public synchronized void decrementRunningThreads() {
		setCurrentRunningThreads(getCurrentConnections()-1);
	}

	public int getMaxSimultaneousConnections() {
		return maxSimultaneousConnections;
	}

	public void setMaxSimultaneousConnections(int maxSimultaneousConnections) {
		this.maxSimultaneousConnections = maxSimultaneousConnections;
	}
	public void close() throws IOException {
		getServSocket().close();
		for(Thread t:sessions.values()) {
			t.interrupt();
		}
	}
	protected Thread getNewClientThread(TCPClientSocket clientSocket) {
		return new ClientThread(clientSocket,getRunnable(clientSocket),this);
	}
	
	protected void begin() {
		
	}
	public void addSession(Thread t) {
		long id=t.getId();
		sessions.put(id,t);
		println("Session with id '"+id+"' added");

	}
	public void removeSession(Thread t) {
		removeSession(t.getId());		
	}
	public void removeSession(long id) {
		Thread removed=sessions.remove(id);
		if(removed!=null) {
			println("Session with id '"+id+"' removed");
		}
		else {
			println("Session with id '"+id+"' NOT removed");
		}
	}
	public void run() {
		TCPClientSocket clientSocket;
		Thread session;
		println("Running "+getName()+" at "+servSocket.getCompleteInputAddr());
		try {
			begin();
			while(isActive() &&(clientSocket=servSocket.accept())!=null) {
				session=getNewClientThread(clientSocket);
				addSession(session);
				session.start();
			}
		}
		catch (IOException e) {
			println(getName());
			getPrinter().printStackTrace(e);
		}
		finally {
			try {
				close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
