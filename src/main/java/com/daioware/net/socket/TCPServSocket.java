package com.daioware.net.socket;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.security.Security;

import javax.net.ssl.SSLServerSocketFactory;

import com.sun.net.ssl.internal.ssl.Provider;

public class TCPServSocket implements Socket {
	public static final int MAX_DEFAULT_CONNECTIONS=50;
	
	private ServerSocket servSocket;
	
	public TCPServSocket(File keyStore,String keyStorePassword,int port) throws UnknownHostException, IOException {
		this(keyStore,keyStorePassword,"0.0.0.0",port,MAX_DEFAULT_CONNECTIONS);
	}

	public TCPServSocket(File keyStore,String keyStorePassword,String address,int port) throws UnknownHostException, IOException {
		this(keyStore,keyStorePassword,address,port,MAX_DEFAULT_CONNECTIONS);
	}
	public TCPServSocket(File keyStore,String keyStorePassword,String address,int port,int maxIncomingConn) throws UnknownHostException, IOException {
		Security.addProvider(new Provider());
		System.setProperty("javax.net.ssl.keyStore",keyStore.getAbsolutePath());
		System.setProperty("javax.net.ssl.keyStorePassword",keyStorePassword);
		SSLServerSocketFactory sslserversocketfactory =
		        (SSLServerSocketFactory)SSLServerSocketFactory.getDefault();		  
		setServSocket(sslserversocketfactory.createServerSocket(port, 
        		maxIncomingConn,InetAddress.getByName(address)));
	}
	public TCPServSocket(String address,int port) throws UnknownHostException, IOException {
		this(address,port,MAX_DEFAULT_CONNECTIONS);
	}
	public TCPServSocket(int port) throws IOException {
		this(new ServerSocket(port)); 
	}
	public TCPServSocket(String address,int port,int maxIncomingConn) throws UnknownHostException, IOException {
		this(new ServerSocket(port, maxIncomingConn,InetAddress.getByName(address))); 
	}
	public TCPServSocket(ServerSocket serv) {
		setServSocket(serv);
	}
	public ServerSocket getServSocket() {
		return servSocket;
	}
	public void setServSocket(ServerSocket servSocket) {
		this.servSocket = servSocket;
	}

	public TCPClientSocket accept() throws IOException {
		return new TCPClientSocket(getServSocket().accept());
	}
	public void close() throws IOException {
		getServSocket().close();
	}
	@Override
	public String getAddr() {
		return getServSocket().getInetAddress().getHostAddress();
	}
	@Override
	public int getOutputPort() {
		return getServSocket().getLocalPort();
	}
	@Override
	public int getInputPort() {
		return getServSocket().getLocalPort();
	}

	@Override
	public String toString() {
		return getCompleteInputAddr();
	}
	
}
