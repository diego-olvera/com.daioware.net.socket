package com.daioware.net.socket;

import com.daioware.stream.Printer;

public class UDPElement{
	protected Printer printer;
	protected UDPSender sender;
	protected UDPReceiver receiver;
	protected int id;
	
	public UDPElement(UDPSender emisor,UDPReceiver receptor) {
		this(1,emisor,receptor);
	}
	public UDPElement(int id,UDPSender emisor,UDPReceiver receptor) {
		this(null,id,emisor,receptor);
	}
	public UDPElement(Printer printer,int id,UDPSender emisor,UDPReceiver receptor) {
		setPrinter(printer);
		setId(id);
		setSender(emisor);
		setReceiver(receptor);
	}

	protected void setSender(UDPSender emisor) {
		this.sender = emisor;
	}

	protected void setReceiver(UDPReceiver receptor) {
		this.receiver = receptor;
	}

	public UDPSender getSender() {
		return sender;
	}
	public UDPReceiver getReceiver() {
		return receiver;
	}
	public int getId() {
		return id; 
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPrinter(Printer p) {
		if(p==null) {
			printer=Printer.emptyPrinter;
		}
		else {
			printer=p;
		}
	}
	public Printer getPrinter(){
		return printer;
	}

}
