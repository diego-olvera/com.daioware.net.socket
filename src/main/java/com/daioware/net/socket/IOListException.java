package com.daioware.net.socket;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class IOListException extends IOException{

	private static final long serialVersionUID = 1L;

	private List<IOException> exceptions;

	
	public IOListException(int initialSize) {
		exceptions=new ArrayList<>(4);

	}
	public IOListException() {
		exceptions=new ArrayList<>();
	}

	public List<IOException> getExceptions() {
		return exceptions;
	}

	public void setExceptions(List<IOException> exceptions) {
		this.exceptions = exceptions;
	}
	@Override
	public void printStackTrace() {
		for(IOException e:exceptions) {
			e.printStackTrace();
		}
	}
	@Override
	public void printStackTrace(PrintStream printStream) {
		for(IOException e:exceptions) {
			e.printStackTrace(printStream);
		}
	}
	
	@Override
	public void printStackTrace(PrintWriter printWriter) {
		for(IOException e:exceptions) {
			e.printStackTrace(printWriter);
		}
	}
	
	
}
