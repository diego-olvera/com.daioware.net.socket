package com.daioware.net.socket;

import java.io.IOException;

public class ClientThread extends Thread{	
	protected TCPClientSocket socket;
	protected GenericTCPServer server;
	public ClientThread(TCPClientSocket socket,Runnable r,GenericTCPServer server) {
		super(r);
		this.socket=socket;
		this.server=server;
	}
	public void print(Object o) {
		server.print(o);
	}
	public void println(Object o) {
		server.println(o);
	}
	public void run() {
		int currentConn;
		String completeAddr=socket.getCompleteInputAddr();
		server.println("Client socket connected with addr:"+completeAddr);
		if((currentConn=server.getCurrentConnections())<server.getMaxSimultaneousConnections()) {
			server.currentRunningThreads++;
			server.println("Current clients:"+(server.getCurrentConnections()));
			super.run();
			server.removeSession(this);
			server.currentRunningThreads--;
		}
		else {
			server.println("Current clients "+currentConn+" is equals to the limit "+
					server.getMaxSimultaneousConnections());
			server.println("Closing connection to "+completeAddr);
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}		
}
