package com.daioware.net.socket;

import java.io.IOException;

public interface Socket {

	String getAddr();

	int getOutputPort();
	int getInputPort();
	default String getCompleteInputAddr() {
		return getAddr()+":"+getInputPort();
	}
	default String getCompleteOutputAddr() {
		return getAddr()+":"+getOutputPort();
	}
	void close() throws IOException;
	

}