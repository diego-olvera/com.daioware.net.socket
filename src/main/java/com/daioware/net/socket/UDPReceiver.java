package com.daioware.net.socket;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UDPReceiver {
	
	private int port;
	private byte[] buffer;
	private DatagramSocket socket;
	private DatagramPacket packet;
	
	public UDPReceiver(String addr,int port,int bufferSize) throws SocketException, UnknownHostException {
		buffer=new byte[bufferSize];
		getPort(port);
		socket=new DatagramSocket(port,InetAddress.getByName(addr));
		packet=new DatagramPacket(buffer, buffer.length);
	}
	public UDPReceiver(String addr,int port) throws SocketException, UnknownHostException {
		this(addr,port,1024);
	}
	public UDPReceiver(int port) throws SocketException, UnknownHostException {
		this("0.0.0.0",port,1024);
	}
	public UDPReceiver(int port,int bufferSize) throws SocketException, UnknownHostException {
		this("0.0.0.0",port,bufferSize);
	}
	public void close() {
		socket.close();
	}
	public DatagramSocket getSocket() {
		return socket;
	}
	public int getPort() {
		return port;
	}

	public void getPort(int port) {
		this.port = port;
	}

	public byte[] receive() throws IOException{
		socket.receive(packet);
		byte bytes[]=new byte[packet.getLength()];
		for(int i=0,j=bytes.length;i<j;i++){
			bytes[i]=buffer[i];
		}
		return bytes;
	}
	public void receive(byte[] message) throws IOException{
		int i=0;
		for(byte b:receive()){
			message[i++]=b;
		}
	}
	public String getTransmitterAddr(){
		return packet.getAddress().getHostAddress();
	}
	public String receiveString() throws IOException{
		return new String(receive());
	}
	public static String receiveString(int port,int bufferSize) throws SocketException, IOException {
		UDPReceiver rec=new UDPReceiver(port, bufferSize);
		String s=rec.receiveString();
		rec.close();
		return s;
	}
	public static byte[] receive(int port,int bufferSize) throws SocketException, IOException {
		UDPReceiver rec=new UDPReceiver(port, bufferSize);
		byte[]bytes=rec.receive();
		rec.close();
		return bytes;
	}
	public static void receive(int port,byte[]message) throws SocketException, IOException {
		UDPReceiver rec=new UDPReceiver(port, message.length);
		rec.receive(message);
		rec.close();
	}
}
