package com.daioware.net.socket;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class UDPSender {
	private String destAddr;
	private String srcAddr;
	private int port;
	private InetAddress inetAddr;
	
	public UDPSender(int port) throws UnknownHostException {
		this("0.0.0.0",port);
	}
	public UDPSender(UDPSender sender) throws UnknownHostException {
		this(sender.getDestAddr(),sender.getPort());
	}
	public UDPSender clone() {
		try {
			return new UDPSender(this);
		} catch (UnknownHostException e) {
			return null;
		}
	}
	public UDPSender(String destAddr, int port) throws UnknownHostException {
		setDestAddr(destAddr);
		setPort(port);
	}
	public String getDestAddr() {
		return destAddr;
	}
	public void setDestAddr(String dest) throws UnknownHostException {
		setInetAddr(InetAddress.getByName(dest));
		this.destAddr = dest;
	}
	protected InetAddress getInedAddr() {
		return inetAddr;
	}
	protected void setInetAddr(InetAddress i){
		inetAddr=i;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	
	public String getSrcAddr() {
		return srcAddr;
	}
	public void setSrcAddr(String srcAddr) {
		this.srcAddr = srcAddr;
	}
	public void send(byte[] message) throws IOException{ 
		DatagramSocket socket=new DatagramSocket();
		socket.send(new DatagramPacket(message, message.length,getInedAddr(),getPort()));
		socket.close();
	}
	public void send(String message) throws Exception{
		send(message.getBytes());
	}
	public static void send(String destAddr,int port,byte[] message) throws UnknownHostException,IOException {
		new UDPSender(destAddr, port).send(message);
	}
	public static void send(String destAddr,int port,String message) throws UnknownHostException,IOException {
		send(destAddr,port,message.getBytes());
	}

}
