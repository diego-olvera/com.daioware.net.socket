package com.daioware.net.socket;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.Security;

import javax.net.ssl.SSLSocketFactory;

import com.daioware.net.ssl.SSLItem;
import com.sun.net.ssl.internal.ssl.Provider;

public class TCPClientSocket implements com.daioware.net.socket.Socket{
	public static final Charset defaultCharset=Charset.forName("UTF-8");
	
	private Socket socket;
	private BufferedReader reader;
	private DataOutputStream writer;
	private Charset charset;
	private InputStream input;
	private Proxy proxy;
	private boolean secure;
	
	public static Socket createSocket(String addr,int port,Proxy proxy,boolean isSecure) throws IOException {
		if(proxy!=null && !Proxy.NO_PROXY.equals(proxy)) {
			if(isSecure) {
				Socket socket=new Socket(proxy);
				SSLSocketFactory sslsocketfactory = (SSLSocketFactory)SSLSocketFactory.getDefault();
				socket.connect(new InetSocketAddress(addr, port));
				return sslsocketfactory.createSocket(socket,addr,port,false);
			}
			else {
				Socket socket=new Socket(proxy);
				socket.connect(new InetSocketAddress(addr, port));
				return socket;
			}
		}
		else if(isSecure){
			return ((SSLSocketFactory)SSLSocketFactory.getDefault()).createSocket(addr,port);
		}
		else {
			return new Socket(InetAddress.getByName(addr),port);
		}
	}
	
	public TCPClientSocket(String addr,int port,Charset charset) throws UnknownHostException, IOException {
		this(addr,port,charset,Proxy.NO_PROXY,false);
	}
	
	public TCPClientSocket(String addr,int port,Charset charset,Proxy proxy, boolean ssl) throws UnknownHostException, IOException {
		this(createSocket(addr, port, proxy, ssl),charset);
		setProxy(proxy);
		setSecure(ssl);
	}
	
	public TCPClientSocket(String addr,int port) throws UnknownHostException, IOException {
		this(new Socket(InetAddress.getByName(addr),port));
	}
	
	public TCPClientSocket(Socket socket) throws IOException {
		this(socket,defaultCharset);
	}
	
	public TCPClientSocket(Socket socket,Charset charset) throws IOException {		
		setSocket(socket,charset);
		this.charset=charset;
	}
	public TCPClientSocket(File trustStore,String password,String addr,int port,Charset charset) throws UnknownHostException, IOException {
		Security.addProvider(new Provider());
		System.setProperty("javax.net.ssl.trustStore",trustStore.getAbsolutePath());
		System.setProperty("javax.net.ssl.trustStorePassword",password);
		SSLSocketFactory sslsocketfactory = (SSLSocketFactory)SSLSocketFactory.getDefault();
		setSocket(sslsocketfactory.createSocket(addr,port),charset);
		
	}
	public TCPClientSocket(SSLItem item,String addr,int port,Charset charset) throws UnknownHostException, IOException {
		this(item.getKeyFile(),item.getPassword(),addr,port,charset);
	}
	public TCPClientSocket(SSLItem item,String addr,int port) throws UnknownHostException, IOException {
		this(item.getKeyFile(),item.getPassword(),addr,port,defaultCharset);
	}
	public TCPClientSocket(File trustStore,String password,String addr,int port) throws UnknownHostException, IOException {
		this(trustStore,password,addr,port,defaultCharset);
	}
	protected void setSocket(Socket socket) throws IOException {
		setSocket(socket,defaultCharset);
	}
	protected void setSocket(Socket socket,Charset charset) throws IOException {
		if(this.socket!=null) {
			close();
		}
		this.socket = socket;
		reader =
				new BufferedReader(new InputStreamReader(input=socket.getInputStream(),charset));
		writer = new DataOutputStream(socket.getOutputStream());
	}
	
	public Proxy getProxy() {
		return proxy;
	}
	
	public void setProxy(Proxy proxy) {
		this.proxy = proxy;
	}
	
	public Charset getCharset() {
		return charset;
	}

	public Socket getSocket() {
		return socket;
	}
	public boolean isSecure() {
		return secure;
	}
	public void setSecure(boolean secure) {
		this.secure = secure;
	}
	public void write(String s) throws IOException {
		writer.write(s.getBytes(charset));
	}
	public void write(byte[] buff) throws IOException {
		writer.write(buff);
	}
	public void writeByte(byte b) throws IOException {
		writer.writeByte(b);
	}

	public void write(byte[] buff,int offset,int length) throws IOException {
		writer.write(buff, offset, length);
	}
	
	public String readLine() throws IOException {
		return reader.readLine();
	}
	
	public String readString() throws IOException {
		byte buff[]=new byte[1024];
		int length=input.read(buff, 0, 1024);
		return new String(buff,0,length,charset);
	}
	
	public int readByte() throws IOException {
		return input.read();
	}
	
	public int readBytes(byte[] buff) throws IOException {
		return readBytes(buff,0,buff.length);
	}
	
	public int readBytes(byte[] buff,int offset,int length) throws IOException {
		return input.read(buff, offset, length);
	}
	
	public void reconnect() throws UnknownHostException, IOException {
		setSocket(createSocket(getAddr(), getOutputPort(), getProxy(), isSecure()),getCharset());
	}
	
	public void setSoTimeout(int millisOfTimeout) throws SocketException {
		getSocket().setSoTimeout(millisOfTimeout);
	}
	public void close() throws IOListException {
		IOListException listException=new IOListException(4);
		try{
			reader.close();
		}catch(IOException e) {
			listException.getExceptions().add(e);
		}
		try{
			writer.close();
		}catch(IOException e) {
			listException.getExceptions().add(e);
		}
		try {
			input.close();
		}catch(IOException e) {
			listException.getExceptions().add(e);
		}
		try {
			socket.close();
		}catch(IOException e) {
			listException.getExceptions().add(e);
		}
		if(listException.getExceptions().size()>=1) {
			throw listException;
		}
	}

	@Override
	public String getAddr() {
		return getSocket().getInetAddress().getHostAddress();
	}
	@Override
	public int getOutputPort() {
		return getSocket().getPort();
	}
	public int getInputPort() {
		return getSocket().getLocalPort();
	}
}
